# pspsim - Simulator für das Entwicklungsboard aus dem Praktikum Systemprogrammierung (PSP)
Ein Wrapper um Jonas Bröckmanns [avrsimv2](https://git.rwth-aachen.de/jonas.broeckmann/avrsimv2) gepackaged für Debian/Ubuntu.

## Installation
Die `.deb`-Datei [hier](https://git.rwth-aachen.de/qnemann/pspsim/-/releases) herunterladen und wie folgt installieren:
```
$ sudo apt install ./pspsim-0.1.deb
```

## Benutzung
Dem Tools wird eine `.elf`-Datei übergeben:
```
$ pspsim ./SPOS.elf
```

### Debugging
Um den GDB Server zum Debuggen zu starten muss der Port über die `-g` Option angegeben werden. Z.B.:
```
$ pspsim -g 1234 ./SPOS.elf
```

Dieser kann dann z.B. direkt mit GDB angesprochen werden:
```
$ avr-gdb SPOS.elf
Reading symbols from SPOS.elf...done.
(gdb) target remote :1234
Remote debugging using :1234
0x00000000 in __vectors ()
(gdb)
```

(Bemerkung: Wenn ein Port zum Debuggen angegeben ist wird der Simulator direkt zu Beginn pausiert, um das Setzen von Breakpoints etc. zu ermöglichen. Die Ausführung muss dann also beispielsweise in GDB mit einem `continue` fortgesetzt werden.)
