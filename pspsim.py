#!/usr/bin/env python3
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument('file')
parser.add_argument('-g', default=None)
parser.add_argument('--v0', action='store_true', help='choose board configuration for Versuch0')

args = parser.parse_args()

port = args.g
full_path = os.path.abspath(args.file)

this_dir = os.path.dirname(os.path.realpath(__file__))
os.chdir(os.path.join(this_dir, 'avrsimv2'))

sim_args = [
    './avrsimv2', '--mmcu', 'atmega644', '--freq', '20000000', '-x'
]

if args.v0:
    sim_args += ['--board', 'boards/board_xml.so', '--board-arg', 'boards/psp_V0V2-V5.xml']
else:
    sim_args += ['--board', 'boards/psp_board.so']

if port is not None:
    sim_args += ['-g', port]

sim_args.append(full_path)

os.execv('./avrsimv2', sim_args)
