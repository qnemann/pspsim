.PHONY: package
package :
	-mkdir -p ./package/usr/share/pspsim/avrsimv2/boards ./package/usr/bin
	-cp pspsim.py ./package/usr/share/pspsim
	-ln -s ../share/pspsim/pspsim.py ./package/usr/bin/pspsim
	-cp ./avrsimv2/avrsimv2 ./package/usr/share/pspsim/avrsimv2/
	-cp -r ./avrsimv2/res ./package/usr/share/pspsim/avrsimv2/
	-cp ./avrsimv2/boards/*.so ./avrsimv2/boards/*.xml ./avrsimv2/boards/*.xsd ./package/usr/share/pspsim/avrsimv2/boards/
	dpkg-deb --root-owner-group --build package pspsim.deb

.PHONY: clean
clean :
	-rm -r ./package/usr
